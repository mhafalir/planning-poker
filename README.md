# Planning Poker

> Collaborative planning app!
> 
> In order to estimate how complex a task is for a team of developers to complete, a common technique that is used is called "planning poker".
> 
> Each developer gets to vote on a task by giving their estimate as a point.
> The set of points you can cast your vote on is usually a predefined set of arbitrary numbers, something like this: 0, 1/2, 1, 2, 3, 5, 8, 13.
> The higher the number, the more complex the task is to complete.
> 
> When everyone has cast their votes, the team can have a discussion about what points the different team members have given to a task.
> 
> This application should allow the team members to vote on a "task" and visualize the results of the vote in real-time.
> 
> The users of the application should be able to do the following:
> 
> 
> Create a poll. A user creates a poll and can share this with other people (this could for example be a code or a link).
> Join a pre-existing poll created by you or someone else. You should be able to cast your vote on different points in a predefined set (0, 1/2, 1, 2, 3, 5, 8, 13). A user can only vote once, but is permitted to change their vote.
> Visualize the results in real-time of a poll: Anyone should be able to see the results of a poll. If user A is casting a vote "2", user B should in real-time be able to see that the point "2" has a value of 1.
> You are allowed to use any language/frameworks/libraries you want. 
> 
> We would like you to send in the code (e.g. as a link to github) the day before the interview. The app should come with clear and simple instructions for how to run it on a Mac.



## Tech Stack / Architecture

- Python
- Flask
- MongoDB
- Redis
- React
- Socket io
- Mobx
- Logstash, Kibana, Elasticsearch

Flask application only serves a single page and single api to create poker session. Rest of the work is done by socket io and react.

## Installation / Production Setup

*Not implemented yet*

## Development Setup

**MongoDB, Redis and ELK should be installed and running on your local environment**

1. Create and activate a virtual environment for Python 3.6.x
2. If you don't have MongoDB, Redis and ELK installed and running, you can use docker-compose alternatively. You should navigate to src directory from project root and run:

   `docker-compose up`

3. Navigate to src directory from project root and run:

   `npm install`

   `npm run webpack`

6. In another terminal tab (in src directory):

   `pip install -r requirements.txt`

   `export FLASK_APP=app.py`

   `export FLASK_DEBUG=1`

   `flask run`

13. Go to: http://localhost:5000/

## Tests

Navigate to src directory from project root and run:

`python -m unittest discover`

Only implemented some tests for `PokerService`

Frontend / Karma tests are missing.

## Continuous Integration / Continuous Deployment

*Not implemented yet*

## Error Handling / Logging / Monitoring / APM

Used Logstash, Elasticsearch, Kibana stack for logging

Handled exceptions and throttled actions are ported to Logstash through UDP

## Scaling

MongoDB and Redis are distributed and horizontally scalable by nature, so is Elasticsearch in ELK stack (Logstash and Kibana will not need to be scaled).

However, in order to scale the application, since clients should be syncronized through sockets, these distributed web applications (nodes) should also be sycronized. Suggested way of providing this capability is using sub/pub over redis. Did not implemented it yet though.

## Concurrency

Although `mongoengine` tracks changes to documents to provide efficient saving, any change is updated atomically, still, concurrency issues and race conditions persist. This issue should be improved and reconsidered in the application. 

## Caching

Avoided caching since real-time is the most crucial feature of this application.

## Throttling

Used `flask_limiter` for rate limiting of creating poker session (currently 50 per hour). `flask_limiter` uses redis as backend, and also ports throttling logs into ELK.

## Frontend Related

Used webpack to build frontend react components. For every build, build hash should be appended to the bundle file in order to avoid browser caching. Imported `HtmlWebpackPlugin` however couldn't implemented yet.

## Further Improvements

- Because of time limitation, lots of possible features are not implemented, such as: editing/removing stories, restricting specific actions for only moderators, etc. There are also some more significant features missing, such as removing a user from session when user disconnects, etc.

## Demo

Not available yet unfortunately, but here is a screen capture from local development environment to give a sense of what it is look like:

https://vimeo.com/292870464

## Notes

### Document Structure:
```
{  
    "_id":{  
        "$oid":"5bb2c1946303a91d6ec6789a"
    },
    "key":"30792e9a-170c-40ea-818a-02e7876c9776",
    "users":[  
        {  
            "key":"c8c56c63-9ceb-4272-852f-62d27c1cb74b",
            "name":"User1",
            "moderator":true
        },
        {  
            "key":"e86bbc8b-c87d-4faf-8135-aabc3172aeeb",
            "name":"User2"
        },
        {  
            "key":"96ae1259-94ba-4980-bce4-2ae7f06f1801",
            "name":"User3"
        }
    ],
    "stories":[  
        {  
            "description":"Story1",
            "key":"1c0741e4-6bef-42e3-b637-0f75e3281c49",
            "active":false,
            "complete":true,
            "votes":[  
                {  
                    "user":{  
                        "key":"96ae1259-94ba-4980-bce4-2ae7f06f1801",
                        "name":"User3"
                    },
                    "vote":"3"
                },
                {  
                    "user":{  
                        "key":"e86bbc8b-c87d-4faf-8135-aabc3172aeeb",
                        "name":"User2"
                    },
                    "vote":"5"
                },
                {  
                    "user":{  
                        "key":"c8c56c63-9ceb-4272-852f-62d27c1cb74b",
                        "name":"User1",
                        "moderator":true
                    },
                    "vote":"3"
                }
            ]
        },
        {  
            "description":"Story2",
            "key":"2bd38ec5-1511-4742-b2dc-da0dadb3aad2",
            "active":true,
            "votes":[  
                {  
                    "user":{  
                        "key":"e86bbc8b-c87d-4faf-8135-aabc3172aeeb",
                        "name":"User2"
                    },
                    "vote":"?"
                },
                {  
                    "user":{  
                        "key":"c8c56c63-9ceb-4272-852f-62d27c1cb74b",
                        "name":"User1",
                        "moderator":true
                    },
                    "vote":"?"
                }
            ]
        },
        {  
            "description":"Story3",
            "key":"35e819e6-e8ab-4874-acc6-44950212b8dc",
            "active":false,
            "votes":[  

            ]
        },
        {  
            "description":"Story4",
            "key":"60508f8b-e128-4334-b73c-bab0260972bc",
            "active":false,
            "votes":[  

            ]
        }
    ]
}
```