import os

class Config(object):
    DEBUG = False
    TESTING = False
    MONGODB_HOST = 'localhost'
    MONGODB_DB = 'planning-poker'
    LOGSTASH_HOST = 'localhost'
    LOGSTASH_PORT = 5959
    REDIS_HOST = 'localhost'

class DockerConfig(Config):
    MONGODB_HOST = 'mongo'
    LOGSTASH_HOST = 'elk'
    REDIS_HOST = 'redis'

class DevelopmentConfig(Config):
    pass

class ProductionConfig(Config):
    pass

class TestConfig(Config):
    MONGODB_HOST = 'mongomock://localhost'

configs = {
    'production': ProductionConfig(),
    'development': DevelopmentConfig(),
    'docker': DockerConfig(),
    'test': TestConfig,
}

ENV = os.environ.get('ENV', 'development')

config = configs[ENV]
