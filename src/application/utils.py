import logging
import logstash
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address

from application.configuration import config

logger = logging.getLogger(__name__)
logstash_handler = logstash.LogstashHandler(config.LOGSTASH_HOST, config.LOGSTASH_PORT, version=1)
logger.addHandler(logstash_handler)

limiter = Limiter(key_func=get_remote_address, storage_uri='redis://{}:6379'.format(config.REDIS_HOST))
limiter.logger.addHandler(logstash_handler)
