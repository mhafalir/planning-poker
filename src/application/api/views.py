from flask import Blueprint, request, Response, jsonify
from flask_api import status
from mongoengine import connect
from bson.objectid import ObjectId

from application.core.services import PokerService
from application.utils import limiter


mod = Blueprint('api', __name__, url_prefix='/api')

@mod.route('/poker', methods=['PUT'])
@limiter.limit('50 per hour')
def create():
    poker = PokerService.create(request.cookies.get('user-session'))
    return poker.to_json(), status.HTTP_200_OK
