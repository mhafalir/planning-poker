from flask import request
from flask_api import status
from flask_socketio import SocketIO, emit, join_room, leave_room
from mongoengine import connect
from bson.objectid import ObjectId
import uuid

from application.core.services import PokerService

socketio = SocketIO(manage_session=False)

@socketio.on('join')
def join(user_session, poker_session):
    join_room(poker_session)
    with PokerService(poker_session, user_session) as service:
        poker = service.join()
    emit('updated', poker.to_json(), room=poker_session)

@socketio.on('set-name')
def set_name(user_session, poker_session, name):
    with PokerService(poker_session, user_session) as service:
        poker = service.set_name(name)
    emit('updated', poker.to_json(), room=poker_session)

@socketio.on('add-story')
def add_story(user_session, poker_session, description):
    with PokerService(poker_session, user_session) as service:
        poker = service.add_story(description)
    emit('updated', poker.to_json(), room=poker_session)

@socketio.on('set-story')
def set_story(user_session, poker_session, key):
    with PokerService(poker_session, user_session) as service:
        poker = service.set_story(key)
    emit('updated', poker.to_json(), room=poker_session)

@socketio.on('vote')
def vote(user_session, poker_session, story_key, value):
    with PokerService(poker_session, user_session) as service:
        poker = service.vote(story_key, value)
    emit('updated', poker.to_json(), room=poker_session)

@socketio.on('show-votes')
def show_votes(user_session, poker_session, story_key):
    with PokerService(poker_session, user_session) as service:
        poker = service.show_votes(story_key)
    emit('updated', poker.to_json(), room=poker_session)
