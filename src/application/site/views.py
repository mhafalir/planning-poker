from flask import Blueprint, render_template, request, current_app
import uuid

mod = Blueprint('site', __name__, url_prefix='', template_folder='templates')

@mod.route('/', defaults={'path': ''})
@mod.route('/poker/<path:path>')
def main(path):
    response = current_app.make_response(render_template('site/index.html'))
    if not request.cookies.get('user-session'):
        response.set_cookie('user-session', str(uuid.uuid4()))
    return response
