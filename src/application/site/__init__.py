
def register_views(app):
    from .web.site.views import mod as site_views
    from .web.api.views import mod as dashboard_views
    from .web.socket.views import mod as admin_views
    app.register_blueprint(site_views)
    app.register_blueprint(dashboard_views)
    app.register_blueprint(admin_views)

def register_apis(app):
    from .web.admin.apis import mod as admin_api
    from .web.auth.apis import mod as auth_api
    from .web.api.apis import mod as public_api
    app.register_blueprint(admin_api)
    app.register_blueprint(auth_api)
    app.register_blueprint(public_api)
