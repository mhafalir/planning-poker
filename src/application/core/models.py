import datetime
from mongoengine import (BooleanField, DateTimeField, DictField, Document,
                         EmailField, IntField, ListField, ObjectIdField, ReferenceField,
                         StringField, EmbeddedDocument, EmbeddedDocumentField)

class User(EmbeddedDocument):
    key = StringField(max_length=255)
    name = StringField(max_length=255)
    moderator = BooleanField()


class Vote(EmbeddedDocument):
    user = EmbeddedDocumentField(User)
    vote = StringField(max_length=16)


class Story(EmbeddedDocument):
    description = StringField(max_length=255)
    key = StringField(max_length=255)
    active = BooleanField()
    complete = BooleanField()
    result = StringField(max_length=16)
    votes = ListField(EmbeddedDocumentField(Vote))


class PokerSession(Document):
    key = StringField(max_length=255)
    users = ListField(EmbeddedDocumentField(User))
    stories = ListField(EmbeddedDocumentField(Story))
