import unittest
from unittest.mock import Mock, patch

from application.core.services import PokerService

class PokerSessionTestCase(unittest.TestCase):

    def test_creates_poker_session(self):
        poker = PokerService.create('userkey')
        self.assertIsNotNone(poker)
        self.assertEqual(1, len(poker.users))
        self.assertTrue(poker.users[0].moderator)

    def test_user_joines_poker_session_only_once(self):
        poker = PokerService.create('userkey')
        with PokerService(poker.key, 'new_user_key') as service:
            poker = service.join()
            self.assertEqual(2, len(poker.users))
            self.assertEqual('new_user_key', poker.users[1].key)
            poker = service.join()
            self.assertEqual(2, len(poker.users))
