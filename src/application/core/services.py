from mongoengine import connect
import uuid

from application.core.models import PokerSession, User, Story, Vote
from application.configuration import config
from application.utils import logger


class PokerService():

    def __init__(self, session_key, user_key):
        self.session_key = session_key
        self.user_key = user_key
    
    def __enter__(self):
        try:
            self.connection = connect(config.MONGODB_DB, host=config.MONGODB_HOST, alias='default')
            self.poker = PokerSession.objects.get(key=self.session_key)
        except Exception:
            logger.exception('PokerService.__enter__')
        return self
    
    def __exit__(self, type, value, traceback):
        self.connection.close()

    def _hide_incomplete_results(self):
        for story in self.poker.stories:
            if not story.complete:
                for vote in story.votes:
                    vote.vote = "?"
        return self.poker

    @staticmethod
    def create(user_key):
        try:
            with connect(config.MONGODB_DB, host=config.MONGODB_HOST, alias='default'):
                poker = PokerSession()
                poker.key = str(uuid.uuid4())
                poker.users = []
                user = User()
                user.moderator = True
                user.key = user_key
                poker.users.append(user)
                poker.save()
                return poker
        except Exception:
            logger.exception('PokerService.create')
            return None


    def join(self):
        try:
            if self.user_key not in [u.key for u in self.poker.users]:
                user = User()
                user.key = self.user_key
                self.poker.users.append(user)
                self.poker.save()
            return self._hide_incomplete_results()
        except Exception:
            logger.exception('PokerService.join')
            return self.poker

    def set_name(self, name):
        try:
            for user in self.poker.users:
                if user.key == self.user_key:
                    user.name = name
                    self.poker.save()
                    break
            return self._hide_incomplete_results()
        except Exception:
            logger.exception('PokerService.set_name')
            return self.poker

    def add_story(self, description):
        try:
            story = Story()
            story.description = description
            story.key = str(uuid.uuid4())
            self.poker.stories.append(story)
            self.poker.save()
            return self._hide_incomplete_results()
        except Exception:
            logger.exception('PokerService.add_story')
            return self.poker

    def set_story(self, key):
        try:
            for story in self.poker.stories:
                if story.key == key:
                    story.active = True
                else:
                    story.active = False
            self.poker.save()
            return self._hide_incomplete_results()
        except Exception:
            logger.exception('PokerService.set_story')
            return self.poker

    def vote(self, story_key, value):
        try:
            story = [s for s in self.poker.stories if s.key == story_key and s.active][0]
            user = [u for u in self.poker.users if u.key == self.user_key][0]
            already_voted = False
            for vote in story.votes:
                if vote.user.key == self.user_key:
                    if vote.vote == value:
                        story.votes.remove(vote)
                        already_voted = True
                        break
                    vote.vote = str(value)
                    already_voted = True
                    break
            if not already_voted:
                vote = Vote()
                story.votes.append(vote)
                self.poker.save()
                vote.vote = str(value)
                vote.user = user
            self.poker.save()
            return self._hide_incomplete_results()
        except Exception:
            logger.exception('PokerService.vote')
            return self.poker

    def show_votes(self, story_key):
        try:
            story = [s for s in self.poker.stories if s.key == story_key and s.active][0]
            story.complete = True
            self.poker.save()
            return self._hide_incomplete_results()
        except Exception:
            logger.exception('PokerService.show_votes')
            return self.poker
