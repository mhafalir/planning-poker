from flask import Flask

from application.site.views import mod as site_module
from application.api.views import mod as api_module
from application.socket.views import socketio
from application.utils import limiter


app = Flask(__name__)
app.config['SECRET_KEY'] = 'b16f13ac649b4866b77af77c6fbd1c99'
app.register_blueprint(site_module)
app.register_blueprint(api_module)
limiter.init_app(app)
socketio.init_app(app)

if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0')
