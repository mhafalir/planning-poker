var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        site: './static/app.jsx',
    },
    output: {
        path: __dirname + '/static',
        filename: "[name].bundle.js"
    },
    module: {
        rules: [{
            test: /\.(js|jsx)?$/,
            loader: 'babel-loader',
            query: {
                presets: ['es2015', 'react'],
                plugins: ['transform-decorators-legacy', 'transform-class-properties']
            },
            exclude: /node_modules/,
        }, {
            test: /\.css$/, 
            loader: "style-loader!css-loader" 
        }, { test: /\.(png|woff|woff2|eot|ttf|svg)$/,
            loader: 'url-loader?limit=100000'
        }],
    },
    resolve: {
        extensions: ['.wasm', '.mjs', '.js', '.json', '.jsx']
    },
    plugins: [
    ]
};