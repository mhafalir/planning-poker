import React, { Component } from 'react';
import PropTypes from 'prop-types';
import autobind from 'autobind-decorator';
import { inject, observer } from 'mobx-react';
import { Button, ButtonToolbar, Form } from 'react-bootstrap';

@inject('store')
@observer
class Story extends Component {
    static propTypes = {
        store: PropTypes.object.isRequired,
        story: PropTypes.object.isRequired,
    }

    static defaultProps = {
        store: null,
        story: null,
    }

    constructor(props) {
        super(props);
    }

    @autobind setStory() {
        this.props.store.setStory(this.props.story.key);
    }

    render() {
        const voteItems = this.props.story.votes.map((v, i) => (
            <Button key={ i } disabled>{ v.user.name }: { v.vote }</Button>
        ));
        return (
            <div>
                <Form inline>
                    { this.props.story.description }
                    { !this.props.story.active && <Button onClick={ this.setStory } bsSize="sm" className="pull-right">Vote</Button> }
                </Form>
                <hr />
                <ButtonToolbar>
                    { voteItems }
                </ButtonToolbar>
            </div>
        );
    }
}

export default Story;