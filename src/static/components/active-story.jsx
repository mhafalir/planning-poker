import React, { Component } from 'react';
import PropTypes from 'prop-types';
import autobind from 'autobind-decorator';
import { inject, observer } from 'mobx-react';
import { ButtonToolbar, ListGroup, ListGroupItem, Button, Panel } from 'react-bootstrap';

import VoteOptions from '../vote-options';
import VoteButton from './vote-button';


@inject('store')
@observer
class ActiveStory extends Component {
    static propTypes = {
        store: PropTypes.object.isRequired,
    }

    static defaultProps = {
        store: null,
    }

    constructor(props) {
        super(props);

        this.state = {
            'description': ''
        };
    }

    @autobind handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({ 
            [name]: value
        });
    }

    @autobind addStory() {
        this.props.store.addStory(this.state.description);
    }

    @autobind showVotes() {
        const story = this.props.store.poker.stories.find(s => s.active);
        this.props.store.showVotes(story);
    }

    @autobind previousStory() {
        const index = this.props.store.poker.stories.findIndex(s => s.active);
        if (index > 0) {
            this.props.store.setStory(this.props.store.poker.stories[index - 1].key);
        }
    }

    @autobind nextStory() {
        const index = this.props.store.poker.stories.findIndex(s => s.active);
        if (index < this.props.store.poker.stories.length - 1) {
            this.props.store.setStory(this.props.store.poker.stories[index + 1].key);
        }
    }

    render() {
        if (!this.props.store || !this.props.store.poker) {
            return null;
        }
        const story = this.props.store.poker.stories.find(s => s.active);
        if (!story){
            return null;
        }
        const voteItems = story.votes.map((v, i) => (
            <ListGroupItem key={ i }>{ v.user.name }: <strong className="pull-right">{ v.vote }</strong></ListGroupItem>
        ));
        const voteButtonItems = VoteOptions.map((v, i) => (
            <VoteButton key={ i } value={ v } story={ story } />
        ))
        return (
            <Panel>
                <Panel.Heading>
                    <Panel.Title>{ story.description }</Panel.Title>
                </Panel.Heading>
                <Panel.Body>
                    <ButtonToolbar>
                        { voteButtonItems }
                    </ButtonToolbar>
                </Panel.Body>
                <Panel.Footer>
                    <ListGroup>{ voteItems }</ListGroup>
                    <Button className="pull-right" onClick={ this.showVotes }>Show Votes</Button>
                    <div className="clearfix"></div>
                </Panel.Footer>
                <Panel.Footer>
                    <Button className="pull-left" onClick={ this.previousStory }>Previous Story</Button>
                    <Button className="pull-right" onClick={ this.nextStory }>Next Story</Button>
                    <div className="clearfix"></div>
                </Panel.Footer>
            </Panel>
        );
    }
}

export default ActiveStory;