import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import { Glyphicon, ListGroup, ListGroupItem, Panel,  } from 'react-bootstrap';


@inject('store')
@observer
class Users extends Component {
    static propTypes = {
        store: PropTypes.object.isRequired,
    }

    static defaultProps = {
        store: null,
    }

    constructor(props) {
        super(props);

        this.state = {
            'description': ''
        };
    }

    render() {
        if (!this.props.store.poker || !this.props.store.user) {
            return null;
        }
        const userItems = this.props.store.poker.users.map((u, i) => (
            <ListGroupItem key={ i }>{ u.moderator ? <Glyphicon glyph="asterisk" /> : "" } { u.name }</ListGroupItem>
        ));
        return (
            <Panel>
                <Panel.Heading>
                    <Panel.Title>Welcome, { this.props.store.user.name }</Panel.Title>
                </Panel.Heading>
                <Panel.Body>
                    <ListGroup>
                        <div>
                            <ListGroup>
                                { userItems }
                            </ListGroup>
                        </div>
                    </ListGroup>
                </Panel.Body>
                <Panel.Footer>
                    You can invite new users with session code: { this.props.store.sessionKey }
                </Panel.Footer>
            </Panel>
        );
    }
}

export default Users;