import React, { Component } from 'react';
import { Navbar, Grid, Row, Col } from 'react-bootstrap';

class Header extends Component {
    render() {
        return (
            <Grid>
                <Row>
                    <Col>
                        <Navbar>
                            <Navbar.Header>
                                <Navbar.Brand>
                                    <a href="/">Planning Poker</a>
                                </Navbar.Brand>
                            </Navbar.Header>
                        </Navbar>
                    </Col>
                </Row>
            </Grid>
        );
    }
}

export default Header;