import React, { Component } from 'react';
import PropTypes from 'prop-types';
import autobind from 'autobind-decorator';
import { inject, observer } from 'mobx-react';
import { Button } from 'react-bootstrap';


@inject('store')
@observer
class VoteButton extends Component {
    static propTypes = {
        store: PropTypes.object.isRequired,
        value: PropTypes.string.isRequired,
        story: PropTypes.object.isRequired,
    }

    static defaultProps = {
        store: null,
        value: '',
        story: null,
    }

    @autobind vote() {
        this.props.store.vote(this.props.story, this.props.value);
    }

    render() {
        const { store, value, story } = this.props;
        const isActive = story.votes.find(v => v.vote == value && v.user.key == store.user.key) != null;
        return (
            <Button value={ value } onClick={ this.vote } className={ isActive ? "btn-primary" : "" }>{ value }</Button>
        );
    }
}

export default VoteButton;
