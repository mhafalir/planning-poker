import React, { Component } from 'react';
import PropTypes from 'prop-types';
import autobind from 'autobind-decorator';
import { inject, observer } from 'mobx-react';
import { Button, InputGroup, ListGroup, ListGroupItem, Panel, Form, FormGroup, FormControl } from 'react-bootstrap';

import Story from './story';

@inject('store')
@observer
class Stories extends Component {
    static propTypes = {
        store: PropTypes.object.isRequired,
    }

    static defaultProps = {
        store: null,
    }

    constructor(props) {
        super(props);

        this.state = {
            'description': ''
        };
    }

    @autobind handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({ 
            [name]: value
        });
    }

    @autobind addStory() {
        this.props.store.addStory(this.state.description);
    }

    render() {
        if (!this.props.store || !this.props.store.poker) {
            return null;
        }
        const storyItems = this.props.store.poker && this.props.store.poker.stories ? this.props.store.poker.stories.map(s => (
            <ListGroupItem key={ s.key }><Story story={ s } /></ListGroupItem>
        )) : ( '' );
        return (
            <Panel>
                <Panel.Heading>
                    <Panel.Title>Stories</Panel.Title>
                </Panel.Heading>
                <Panel.Body>
                    <ListGroup>
                        { storyItems }
                    </ListGroup>
                </Panel.Body>
                <Panel.Footer>
                    <Form>
                        <FormGroup>
                            <InputGroup>
                                <FormControl type="text" placeholder="description" name="description" onChange={ this.handleInputChange } />
                                <InputGroup.Button>
                                    <Button onClick={ this.addStory }>Add Story</Button>
                                </InputGroup.Button>
                            </InputGroup>
                        </FormGroup>
                    </Form>
                </Panel.Footer>
            </Panel>
        );
    }
}

export default Stories;