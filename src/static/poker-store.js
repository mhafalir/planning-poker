import { action, observable, computed } from 'mobx';
import axios from 'axios';
import openSocket from 'socket.io-client';
import { NotificationManager } from 'react-notifications';


export default class PokerStore {
    sessionKey = null;
    userSessionKey = null;
    @observable poker = null;

    @computed get user() {
        return this.poker ? this.poker.users.find(u => u.key == this.userSessionKey) : '';
    }

    constructor(sessionKey, userSessionKey) {
        this.sessionKey = sessionKey;
        this.userSessionKey = userSessionKey;
        this.socket = openSocket('/');
        this.socket.on('updated', data => {
            this.poker = JSON.parse(data);
            console.log(data);
        });
        this.socket.on('connect', () => {
            NotificationManager.success('Connected');
        });
        this.socket.on('connect_error', error => {
            NotificationManager.error(`Connection Error: ${error}`);
        });
        this.init();
    }

    @action init() {
        this.socket.emit('join', this.userSessionKey, this.sessionKey);
    }

    @action setName(name) {
        this.socket.emit('set-name', this.userSessionKey, this.sessionKey, name);
    }

    @action addStory(description) {
        this.socket.emit('add-story', this.userSessionKey, this.sessionKey, description);
    }

    @action setStory(storyKey) {
        this.socket.emit('set-story', this.userSessionKey, this.sessionKey, storyKey);
    }

    @action vote(story, value) {
        this.socket.emit('vote', this.userSessionKey, this.sessionKey, story.key, value);
    }

    @action showVotes(story) {
        this.socket.emit('show-votes', this.userSessionKey, this.sessionKey, story.key);
    }
}
