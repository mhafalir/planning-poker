import React, { Component } from 'react';
import PropTypes from 'prop-types';
import autobind from 'autobind-decorator';
import { observer, Provider } from 'mobx-react';
import { withCookies, Cookies } from 'react-cookie';
import { Grid, Row, Button, Col, Badge, Modal, Tab, Form, FormGroup, FormControl, Nav, NavItem } from 'react-bootstrap';

import PokerStore from '../poker-store';
import Stories from '../components/stories';
import ActiveStory from '../components/active-story';
import Users from '../components/users';

import 'react-notifications/lib/notifications.css';

@observer
class Poker extends Component {
    static propTypes = {
        sessionKey: PropTypes.string.isRequired,
        cookies: PropTypes.instanceOf(Cookies).isRequired,
    }

    static defaultProps = {
        sessionKey: '',
    }

    constructor(params) {
        super(params);
        this.store = new PokerStore(this.props.match.params.sessionKey, this.props.cookies.get('user-session'));

        this.state = {
            'name': ''
        };
    }

    @autobind handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({ 
            [name]: value
        });
    }

    @autobind setName(event) {
        this.store.setName(this.state.name);
    }

    render() {
        if (!this.store.poker) {
            return null;
        }
        return (
            <Provider store={ this.store }>
                <Grid>
                    <Row>
                        <Col>
                            <Tab.Container id="tabs-with-dropdown" defaultActiveKey="first">
                                <Row className="clearfix">
                                    <Col sm={12}>
                                        <Nav bsStyle="pills">
                                            <NavItem eventKey="first">Users <Badge>{ this.store.poker.users.length }</Badge></NavItem>
                                            <NavItem eventKey="second">Stories <Badge>{ this.store.poker.stories.length }</Badge></NavItem>
                                            <NavItem eventKey="third">Vote</NavItem>
                                        </Nav>
                                    </Col>
                                    <Col sm={12}>
                                        <Tab.Content animation>
                                            <Tab.Pane eventKey="first">
                                                <Users />
                                            </Tab.Pane>
                                            <Tab.Pane eventKey="second">
                                                <Stories />
                                            </Tab.Pane>
                                            <Tab.Pane eventKey="third">
                                                <ActiveStory />
                                            </Tab.Pane>
                                        </Tab.Content>
                                    </Col>
                                </Row>
                            </Tab.Container>
                        </Col>
                    </Row>
                    <Modal show={ this.store.user && !this.store.user.name }>
                        <Modal.Header closeButton>
                            <Modal.Title>Your name, please.</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Form>
                                <FormGroup controlId="formInlineEmail">
                                    <FormControl type="text" placeholder="name" name="name" onChange={ this.handleInputChange } />
                                </FormGroup>{' '}
                            </Form>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button bsStyle="primary" onClick={ this.setName }>Continue</Button>
                        </Modal.Footer>
                    </Modal>
                </Grid>
            </Provider>
        );
    }
}

export default withCookies(Poker);