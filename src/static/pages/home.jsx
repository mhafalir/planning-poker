import React, { Component } from 'react';
import { runInAction, observable } from 'mobx';
import { observer } from 'mobx-react';
import { instanceOf } from 'prop-types';
import autobind from 'autobind-decorator';
import { withCookies, Cookies } from 'react-cookie';
import { Grid, Row, Button, Col, Panel, Form, FormGroup, FormControl } from 'react-bootstrap';
import axios from 'axios';
import { NotificationManager } from 'react-notifications';

@observer
class Home extends Component {
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired,
    }

    constructor(props) {
        super(props);

        this.state = {
            'code': ''
        };
    }

    @autobind handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({ 
            [name]: value
        });
    }

    @autobind enterSession() {
        this.props.history.push(`/poker/${this.state.code}`);
    }

    @autobind createNewSession() {
        axios.put('/api/poker').then(response => {
            this.props.history.push(`/poker/${response.data.key}`);
        }).catch(error => {
            NotificationManager.error(`Error while creating poker session: ${error}`);
        });
    }

    render() {
        return (
            <Grid>
                <Row>
                    <Col>
                        <Panel>
                            <Panel.Body className="text-center">
                                <Form>
                                    <Button onClick={ this.createNewSession }>Create New Session</Button>
                                </Form>
                            </Panel.Body>
                        </Panel>
                        <Panel>
                            <Panel.Body className="text-center">
                                <Form inline>
                                    <FormGroup controlId="formInlineEmail">
                                        <FormControl type="text" placeholder="code" name="code" onChange={ this.handleInputChange } />
                                    </FormGroup>{' '}
                                    <Button onClick={ this.enterSession }>Enter a Session</Button>
                                </Form>
                            </Panel.Body>
                        </Panel>
                    </Col>
                </Row>
            </Grid>
        );
    }
}

export default withCookies(Home);