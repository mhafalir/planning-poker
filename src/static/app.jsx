import React from 'react';
import ReactDOM from 'react-dom';
import { CookiesProvider } from 'react-cookie';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { NotificationContainer } from 'react-notifications';
import Home from './pages/home';
import Poker from './pages/poker';
import Header from './components/header';

ReactDOM.render((
    <CookiesProvider>
        <Header />
        <BrowserRouter>
            <Switch>
                <Route exact path='/' component={ Home } />
                <Route path='/poker/:sessionKey' component={ Poker } />
            </Switch>
        </BrowserRouter>
        <NotificationContainer />
    </CookiesProvider>
), document.getElementById('app'));